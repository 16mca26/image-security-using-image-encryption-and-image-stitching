﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="a_reg_mng_doc.aspx.cs" Inherits="a_reg_mng_doc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style5
        {
            width: 227px;
        }
        .style6
        {
            width: 566px;
        }
        .style9
        {
            width: 326px;
        }
        .style11
        {
            width: 333px;
        }
        .style12
        {
            width: 317px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="style1">
        <tr>
            <td>
                <table class="style1">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:MultiView ID="MultiView1" runat="server">
                                <asp:View ID="View1" runat="server">
                                    <table class="style1">
                                        <tr>
                                            <td class="style5">
                                                &nbsp;</td>
                                            <td>
                                                <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                                                    Height="147px" style="margin-left: 0px" Width="372px" 
                                                    onitemcommand="DataGrid1_ItemCommand">
                                                    <Columns>
                                                        <asp:BoundColumn DataField="ID" HeaderText="ID"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="NAME" HeaderText="NAME"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="SPECIALITY" HeaderText="SPECIALITY">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="address" HeaderText="ADDRESS"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="MAIL-ID" HeaderText="MAIL-ID"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="phone" HeaderText="PHONE"></asp:BoundColumn>
                                                        <asp:ButtonColumn Text="MANAGE"></asp:ButtonColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style5">
                                                &nbsp;</td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View2" runat="server">
                                    
                                    <table class="style1">
        <tr>
            <td class="style9">
                &nbsp;</td>
            <td class="style11">
                &nbsp;</td>
            <td class="style12">
                &nbsp;</td>
            <td class="style6">
                &nbsp;</td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="style9">
                &nbsp;</td>
            <td class="style11">
                <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
            </td>
            <td class="style12">
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="TextBox1" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td class="style6">
            &nbsp;&nbsp;&nbsp;
                </td>
        </tr>
        <tr>
            <td class="style9">
                &nbsp;</td>
            <td class="style11">
                <asp:Label ID="Label2" runat="server" Text="Specialities"></asp:Label>
            </td>
            <td class="style12">
                <asp:DropDownList ID="DropDownList1" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                    ControlToValidate="DropDownList1" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td class="style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style9">
                &nbsp;</td>
            <td class="style11">
                <asp:Label ID="Label3" runat="server" Text="Address"></asp:Label>
            </td>
            <td class="style12">
                <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="TextBox2" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td class="style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style9">
                &nbsp;</td>
            <td class="style11">
                <asp:Label ID="Label5" runat="server" Text="phoneNo"></asp:Label>
            </td>
            <td class="style12">
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="TextBox3" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
            &nbsp;&nbsp;
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                    ControlToValidate="TextBox3" ErrorMessage="**" ForeColor="Red" 
                    ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
            </td>
            <td class="style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style9">
                &nbsp;</td>
            <td class="style11">
                <asp:Label ID="Label4" runat="server" Text="Mail-Id"></asp:Label>
            </td>
            <td class="style12">
                <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="TextBox4" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
&nbsp;&nbsp;
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                    ControlToValidate="TextBox4" ErrorMessage="**" ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </td>
            <td class="style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style9">
                &nbsp;</td>
            <td class="style11">
                &nbsp;</td>
            <td class="style12">
&nbsp;&nbsp;
                </td>
            <td class="style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style9">
                &nbsp;</td>
            <td class="style11">
                <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                    Text="REGISTER DOCTOR" />
            </td>
            <td class="style12">
                <asp:Button ID="Button2" runat="server" onclick="Button2_Click" Text="UPDATE" 
                    Width="168px" />
            </td>
            <td class="style6">
                <asp:Button ID="Button3" runat="server" onclick="Button3_Click1" Text="DELETE" 
                    Width="139px" />
            </td>
        </tr>
    </table>
                                    
                                </asp:View>
                            </asp:MultiView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
</asp:Content>


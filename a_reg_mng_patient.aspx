﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="a_reg_mng_patient.aspx.cs" Inherits="a_reg_mng_patient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 294px;
        }
        .style3
        {
            width: 337px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="style1">
        <tr>
            <td>
                <table class="style1">
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:MultiView ID="MultiView1" runat="server">
                                <asp:View ID="View1" runat="server">
                                    <table class="style1">
                                        <tr>
                                            <td class="style2">
                                                &nbsp;</td>
                                            <td>
                                                <asp:DataGrid ID="DataGrid1" runat="server" style="margin-left: 0px" 
                                                    AutoGenerateColumns="False" onitemcommand="DataGrid1_ItemCommand">
                                                    <Columns>
                                                        <asp:BoundColumn DataField="ID" HeaderText="ID"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="pname" HeaderText="NAME"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="mail" HeaderText="MAIL-ID"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="address" HeaderText="ADDRESS"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="phone" HeaderText="CONTACT"></asp:BoundColumn>
                                                        <asp:ButtonColumn Text="VIEW"></asp:ButtonColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style2">
                                                &nbsp;</td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View2" runat="server">
                                    
                                    <table class="style1">
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style11">
                &nbsp;</td>
            <td class="style12">
                &nbsp;</td>
            <td class="style6">
                &nbsp;</td>
            <td>
            </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style11">
                <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
            </td>
            <td class="style12">
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="TextBox1" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td class="style6">
            &nbsp;&nbsp;&nbsp;
                </td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style11">
                <asp:Label ID="Label3" runat="server" Text="Address"></asp:Label>
            </td>
            <td class="style12">
                <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="TextBox2" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
            <td class="style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style11">
                <asp:Label ID="Label5" runat="server" Text="phoneNo"></asp:Label>
            </td>
            <td class="style12">
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="TextBox3" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
            &nbsp;&nbsp;
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
                    ControlToValidate="TextBox3" ErrorMessage="**" ForeColor="Red" 
                    ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
            </td>
            <td class="style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style11">
                <asp:Label ID="Label4" runat="server" Text="Mail-Id"></asp:Label>
            </td>
            <td class="style12">
                <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="TextBox4" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
&nbsp;&nbsp;
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                    ControlToValidate="TextBox4" ErrorMessage="**" ForeColor="Red" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            </td>
            <td class="style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style11">
                &nbsp;</td>
            <td class="style12">
&nbsp;&nbsp;
                </td>
            <td class="style6">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style3">
                &nbsp;</td>
            <td class="style11">
                &nbsp;</td>
            <td class="style12">
                <asp:Button ID="Button1" runat="server" Text="REGISTER" 
                    onclick="Button1_Click" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                    Text="UPDATE" Width="80px" />
                &nbsp;
                <asp:Button ID="Button3" runat="server" onclick="Button3_Click" Text="DELETE" 
                    Width="91px" />
            </td>
            <td class="style6">
                &nbsp;</td>
        </tr>
    </table>
                                    
                                </asp:View>
                            </asp:MultiView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        </table>
</asp:Content>


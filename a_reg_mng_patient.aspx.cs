﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class a_reg_mng_patient : System.Web.UI.Page
{
    Class1 c = new Class1();
    static int id;
    static int pid;
    protected void Page_Load(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View1);
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select * from patient";
        DataGrid1.DataSource = c.getdata(cmd);
        DataGrid1.DataBind();

    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {

        MultiView1.SetActiveView(View2);
       

        pid = Convert.ToInt32(e.Item.Cells[0].Text);
        TextBox1.Text = e.Item.Cells[1].Text;
        TextBox2.Text = e.Item.Cells[3].Text;
        TextBox3.Text = e.Item.Cells[4].Text;
        TextBox4.Text = e.Item.Cells[2].Text;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

        SqlCommand cmd = new SqlCommand();
        Random rd = new Random();
        string pass = rd.Next(0000, 9999).ToString();
        cmd.CommandText = "select max(pid) from patient";
        id = c.maxid(cmd);
        cmd.CommandText = "insert into doctor values('" + id + "','" + TextBox1.Text + "','" + TextBox2.Text + "','" + TextBox3.Text + "','" + TextBox4.Text + "')";
        c.execute(cmd);
        cmd.CommandText = "insert into login values('" + id + "','" + TextBox4.Text + "','" + pass + "','patient')";
        c.execute(cmd);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "update patient set address='" + TextBox2.Text + "',phone='" + TextBox3.Text + "' where pid='" + pid + "'";
        c.execute(cmd);
        MultiView1.SetActiveView(View1);
        cmd.CommandText = "select * from patient";
        DataGrid1.DataSource = c.getdata(cmd);
        DataGrid1.DataBind();
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "delete from patient where pid='" + pid + "'";
        c.execute(cmd);
        MultiView1.SetActiveView(View1);
        cmd.CommandText = "select * from patient";
        DataGrid1.DataSource = c.getdata(cmd);
        DataGrid1.DataBind();
    }
}
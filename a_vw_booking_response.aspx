﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="a_vw_booking_response.aspx.cs" Inherits="a_vw_booking_response" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="style1">
    <tr>
        <td>
            &nbsp;</td>
        <td>
                                            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                                                onitemcommand="DataGrid1_ItemCommand" Width="543px" 
                                                
                onselectedindexchanged="DataGrid1_SelectedIndexChanged">
                                                <Columns>
                                                    <asp:BoundColumn DataField="b_id" HeaderText="BookID"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="p_id" HeaderText="PatientID"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="book_date" HeaderText="BookDate"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="did" HeaderText="DoctorID"></asp:BoundColumn>
                                                    <asp:ButtonColumn Text="Approve" CommandName="approve"></asp:ButtonColumn>
                                                    <asp:ButtonColumn CommandName="reject" Text="Reject"></asp:ButtonColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
    </tr>
    </table>
</asp:Content>


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class a_vw_booking_response : System.Web.UI.Page
{
    Class1 c = new Class1();
    static int tid;
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT patient.pname, doctor.name, booking.bid, booking.date FROM booking INNER JOIN patient ON booking.pid = patient.pid INNER JOIN doctor ON booking.did = doctor.did";
        DataGrid1.DataSource = c.getdata(cmd);
        DataGrid1.DataBind();
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "approve")
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select max(token) from booking";
            tid = c.maxid(cmd);
            cmd.CommandText = "update booking set status='approved' and token='"+tid+"' where bid='"+e.Item.Cells[0].Text+"'";
            c.execute(cmd);
            cmd.CommandText = "";
            c.execute(cmd);
        }
        if (e.CommandName == "reject")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "update booking set status='rejected' where bid='" + e.Item.Cells[0].Text + "'";
            c.execute(cmd);
        }
    }
}
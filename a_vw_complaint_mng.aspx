﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="a_vw_complaint_mng.aspx.cs" Inherits="a_vw_complaint_mng" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 179px;
        }
        .style3
        {
            width: 156px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="style1">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <table class="style1">
                            <tr>
                                <td class="style2">
                                    &nbsp;</td>
                                <td>
                                    <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                                        Text="&lt;&lt;" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    &nbsp;</td>
                                <td>
                                    <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                                        Height="161px" onitemcommand="DataGrid1_ItemCommand" Width="332px">
                                        <Columns>
                                            <asp:BoundColumn DataField="com_id" HeaderText="com_id" Visible="False">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="p_id" HeaderText="p_id">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="complaint" HeaderText="complaint"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="reply" HeaderText="reply" Visible="False"></asp:BoundColumn>
                                            <asp:ButtonColumn Text="view"></asp:ButtonColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table class="style1">
                            <tr>
                                <td class="style3">
                                    &nbsp;</td>
                                <td>
                                    <asp:Button ID="Button3" runat="server" onclick="Button3_Click" 
                                        Text="&lt;&lt;" />
                                </td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td class="style3">
                                    &nbsp;</td>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Text="Complaint"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style3">
                                    &nbsp;</td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text="Reply"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox4" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                        ControlToValidate="TextBox4" ErrorMessage="**" ForeColor="Red"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td class="style3">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:Button ID="Button1" runat="server" Text="POST" onclick="Button1_Click" />
                                </td>
                            </tr>
                        </table>
                    </asp:View>
                </asp:MultiView>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>


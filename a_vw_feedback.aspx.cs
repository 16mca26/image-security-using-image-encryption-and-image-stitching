﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class a_vw_feedback : System.Web.UI.Page
{
    Class1 c=new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "SELECT patient.name, feedback.feedback, feedback.date, feedback.fid FROM  feedback INNER JOIN patient ON feedback.pid = patient.pid";
        DataGrid1.DataSource = c.getdata(cmd);
        DataGrid1.DataBind();
    }
}
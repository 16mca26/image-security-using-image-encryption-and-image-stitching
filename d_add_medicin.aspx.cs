﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class a_add_medicin : System.Web.UI.Page
{
    Class1 c = new Class1();
    static int id;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        TextBox2.Text = System.DateTime.Now.ToShortDateString();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select pid,name from patient";
        DropDownList1.DataSource = c.getdata(cmd);
        DropDownList1.DataTextField = "name";
        DropDownList1.DataValueField = "pid";
        DropDownList1.DataBind();
        DropDownList1.Items.Insert(0, "select");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select max(mid) from medicine";
        id = c.maxid(cmd);
        cmd.CommandText = "insert into medicine values('"+id+"','"+Session["id"]+"','"+DropDownList1.SelectedValue+"','"+TextBox1.Text+"','"+TextBox2.Text+"')";
        c.execute(cmd);
    }
}
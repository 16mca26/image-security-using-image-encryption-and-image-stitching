﻿<%@ Page Title="" Language="C#" MasterPageFile="~/doctor.master" AutoEventWireup="true" CodeFile="d_vw_booking_details.aspx.cs" Inherits="d_vw_booking_details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="style1">
                                    <tr>
                                        <td>
                                            <asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                                                Text="&lt;&lt;" />
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                                                onitemcommand="DataGrid1_ItemCommand" Width="543px" 
                                                onselectedindexchanged="DataGrid1_SelectedIndexChanged">
                                                <Columns>
                                                    <asp:BoundColumn DataField="bid" HeaderText="BookID"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="pid" HeaderText="PatientID"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="token" HeaderText="TockenNo"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="date" HeaderText="BookingDate"></asp:BoundColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
</asp:Content>


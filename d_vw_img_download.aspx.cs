﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class d_vw_img_download : System.Web.UI.Page
{
    Class1 c = new Class1();
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText = "select * from img";
        DataGrid1.DataSource=c.getdata(cmd);
        DataGrid1.DataBind();
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        string filename = e.Item.Cells[1].Text;
        filename = Server.MapPath(filename);
        Response.ContentType = ContentType;
        Response.AppendHeader("content-Disposition", "attachment;filename=" + filename);
        Response.WriteFile(filename);

        Response.End();


    }
}